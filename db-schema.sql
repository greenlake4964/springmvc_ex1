
    drop table if exists PRODUCT;
    create table PRODUCT (
        id integer not null auto_increment,
        name varchar(255),
        price decimal(19,2),
        qty integer not null,
        type varchar(255),
        primary key (id)
    );