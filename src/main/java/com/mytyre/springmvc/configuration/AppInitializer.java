package com.mytyre.springmvc.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;


/**
 * 
 *  We can also extends AbstractAnnotationConfigDispatcherServletInitializer,
 *  which is more flexible from design perspective
 */
public class AppInitializer implements WebApplicationInitializer {
//public class AppInitializer implements AbstractDispatcherServletInitializer {
	
	public void onStartup(ServletContext container) throws ServletException {
		Logger log = Logger.getLogger(AppInitializer.class);
		log.info("### AppInitializer");
		
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(AppConfig.class);
		ctx.setServletContext(container);
		ServletRegistration.Dynamic servlet = container.addServlet(
				"dispatcher", new DispatcherServlet(ctx));

		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
	}

}
