package com.mytyre.springmvc.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


/*
 * @Configuration indicates that this class contains one or more bean methods annotated with
 * 
 * @ComponentScan is equivalent to context:component-scan base-package="..." in xml, 
 *  providing with where to look for spring managed beans/classes.
 * 
 * @EnableWebMvc is equivalent to mvc:annotation-driven in XML
 *  
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.mytyre.springmvc" )
public class AppConfig {
	
	Logger log = Logger.getLogger(AppConfig.class);
	
	/**
	 * Method viewResolver configures a view resolver to identify the real view.
	 */
	@Bean
	public ViewResolver viewResolver() {
		log.info("### viewResolver");
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/views/");
		viewResolver.setSuffix(".jsp");

		return viewResolver;
	}
	
	/**
	 * Configure a ResourceBundleMessageSource for JSR303 validation
	 * Spring will search for a file named messages.properties in application class path.
	 */
//	@Bean
//	public MessageSource messageSource() {
//		log.info("### messageSource");
//	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//	    messageSource.setBasename("messages");
//	    return messageSource;
//	}
}

