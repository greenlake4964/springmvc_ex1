package com.mytyre.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mytyre.springmvc.model.Product;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Integer, Product> {

	public Product findById(int id) {
		return getByKey(id);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Product> findAllProducts() {
		Criteria criteria = createEntityCriteria();
		return (List<Product>) criteria.list();
	}
	
	@Transactional	
	public void addProduct(Product product) {
		persist(product);
	}
	
	@Transactional	
	public void deleteProductByName(String name) {
		Query query = getSession().createSQLQuery("delete from Product where name = :name");
		query.setString("name", name);
		query.executeUpdate();
	}
}
