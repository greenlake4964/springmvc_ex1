package com.mytyre.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mytyre.springmvc.dao.ProductDaoImpl;
import com.mytyre.springmvc.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	public ProductController(){
		Logger log = Logger.getLogger(ProductController.class);
		log.info("ProductController");
	}
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	private ProductDaoImpl dao;	
	
	@RequestMapping(value="/list")  
	public String listAllProducts(ModelMap model ) {
		List<Product> products = dao.findAllProducts();
		model.addAttribute("products", products);
	    return ("product");  
	}  
	
	@RequestMapping(value="/add", method = RequestMethod.POST)  
	public String addProduct( HttpServletRequest request, Product product, ModelMap model ) {
		dao.addProduct(product);
		
		List<Product> products = dao.findAllProducts();
		model.addAttribute("products", products);
	    return ("product");  
	}	
	
	@RequestMapping(value="/delete", method = RequestMethod.POST)  
	public String deleteProduct( HttpServletRequest request, Product product, ModelMap model ) {
		dao.deleteProductByName(product.getName());
		
		List<Product> products = dao.findAllProducts();
		model.addAttribute("products", products);
	    return ("product");  
	}	
	
	/***
	 * Basic mapping with variable
	 * eg. ${URL}/product/list1?productId=001
	 *
	 * @param productId
	 * @return
	 */
	@RequestMapping(value="/list1")
	public String findDepatment(@RequestParam("productId") String productId){  	    
	    return ("product");  	  
	}  
	
	/***
	 * Basic mapping with variable (Restful style)
	 * ie. ${URL}/product/list2/001
	 * 
	 * @param productId
	 * @return
	 */
	@RequestMapping(value="/list2/{productId}")
	@ResponseBody
	public String restFindProductByID(@PathVariable String productId){	  
	  return ("Find product with ID: " + productId);  
	  
	}  
	
	/***
	 * Multiple variable in a url (Restful style)
	 * ie. ${URL}/product/list3/001/qty/20
	 * @param productId
	 * @param Qty
	 * @return
	 */
	@RequestMapping(value="/list3/{productId}/qty/{qty}")  
	@ResponseBody
	public String findProductQty(  
	  @PathVariable String productId,  
	  @PathVariable int qty){  
	  
	  return ("Find product with ID: " + productId + " quantity is: " + qty);
	  
	}  
	
	/***
	 * Regular expression sample
	 * ie. ${URL}/product/abc.123
	 * @param textualPart
	 * @param numericPart
	 * @return
	 */
	@RequestMapping(value="/{textualPart:[a-z-]+}.{numericPart:[\\d]+}")  
	@ResponseBody
	public String regularExpression(@PathVariable String textualPart,@PathVariable String numericPart){  	  
	    return ("Textual part: " + textualPart + ", numeric part: " + numericPart);
	}  
}
