<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>My Shop</title>

	<style>
		tr:first-child{
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>


<body>
	<h2>List of Products</h2>	
	<div>
	<table>
		<tr>
			<td>Name</td><td>Price</td><td>Type</td><td>Qty</td><td></td>
		</tr>

		<c:forEach items="${products}" var="product">
		<form:form method="POST" action="/product/delete" modelAttribute="product">					
			<tr>
			<td>${product.name}  <input type="hidden" name="name"  value="${product.name}"/></td>
			<td>${product.price} <input type="hidden" name="price" value="${product.price}"/></td>
			<td>${product.type}  <input type="hidden" name="type"  value="${product.type}"/></td>
			<td>${product.qty}   <input type="hidden" name="qty"   value="${product.qty}"/></td>			
			<td><input type='submit' value="Delete"/></td>									
			</tr>
		</form:form>				
		</c:forEach>				
	</table>
	</div>
	<br/>
	<div>


	<table>
        <tr>
	    	<th>Name</th> 
	    	<th>Price</th>
	    	<th>Type</th>
	    	<th>Qty</th>
	    </tr>	
<%-- 	<form:form method="POST" action="/product/add" modelAttribute="command">	    
		<tr>
			<td><form:input type="text" path="name"/></td>
			<td><form:input type="text" path="price"/></td>
			<td><form:input type="text" path="type"/></td>
			<td><form:input type="text" path="qty"/></td>
			<td><input type="submit" value="Add New Product" /></td>
		</tr>

	</form:form> --%>

	<form method="POST" action="/product/add" modelAttribute="product">	    
		<tr>
			<td><input type="text" name="name"/></td>
			<td><input type="text" name="price"/></td>
			<td><input type="text" name="type"/></td>
			<td><input type="text" name="qty"/></td>
			<td><input type="submit" value="Add New Products" /></td>
		</tr>
	</form>	
		
	</table>

	</div>
</body>
</html>